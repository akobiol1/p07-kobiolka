//
//  ViewController.m
//  NewHangman
//
//  Created by Amanda Kobiolka on 4/28/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import "ViewController.h"
#import "GameOverViewController.h"
#import "FriendsViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize friendButton, noFriendButton;

- (void)viewDidLoad {
    
    
    [[friendButton layer] setBorderWidth:1.0f];
    [[friendButton layer] setBorderColor:[UIColor whiteColor].CGColor];
    
  //  [friendButton setImage:hoverImage forState:UIControlStateHighlighted];
    
    [[noFriendButton layer] setBorderWidth:1.0f];
    [[noFriendButton layer] setBorderColor:[UIColor whiteColor].CGColor];
    
   // [noFriendButton setImage:hoverImage forState:UIControlStateHighlighted];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*-(void) prepareForSegue:(UIStoryboardSegue *) segue sender:(id)sender {
    HangmanViewController *hvc;
    hvc = [segue destinationViewController];
    hvc.userSubmittedWord = textboxButton.text;
    
    
}*/

- (IBAction)playWithAFriendButton:(id)sender {
}

- (IBAction)noFriendsButton:(id)sender {
}
@end
