//
//  HangmanViewController.m
//  NewHangman
//
//  Created by Amanda Kobiolka on 4/28/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "HangmanViewController.h"
#import "GameOverViewController.h"
#import "PhewViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface HangmanViewController ()

@end

@implementation HangmanViewController : UIViewController

@synthesize userSubmittedWord, myLabelArray, myButtonArray, guessesRemainingLabel, shapeArray, guessWordTextbox;

int numGuesses = 8;
NSUInteger wordLength = 0;
NSUInteger numLettersFound = 0;
NSUInteger shapeIndex = 0;
NSString * wordToGuess;
NSArray *preselectedWords;

- (void)viewDidLoad {
    shapeIndex = 0;
    numGuesses = 8;
    numLettersFound = 0;
    myLabelArray = [NSMutableArray array];
    preselectedWords = [NSArray arrayWithObjects: @"pupper", @"raindrop", @"raindrop", nil];
    
    wordLength = [userSubmittedWord length];
    wordToGuess = userSubmittedWord;
    if (wordLength == 0) {
       // NSLog(@"Randomly select a word");
        int randomIndex = arc4random() % (3);
        NSString * randomWord = [preselectedWords objectAtIndex:randomIndex];
        wordLength = [randomWord length];
        wordToGuess = randomWord;
    }
    
    
  //  NSLog(@"Word to guess");
  //  NSLog(wordToGuess);

    
    int x = 10;
    int labelXVal = 40;
    if (wordLength > 6) {
        labelXVal = (300/wordLength);
    }
    
    int y = 250;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chalkboardBackground.jpg"]];

    
    NSString* guessesString = [NSString stringWithFormat:@"Guesses remaining: %i", numGuesses];
    guessesRemainingLabel.text = guessesString;
    
    for(int i = 0; i < wordLength; i++) {

        UILabel *myLabel = [[UILabel alloc]initWithFrame:CGRectMake(x, y, labelXVal, 40)];
        myLabel.text = @"____";
        myLabel.textColor = [UIColor whiteColor];
        [[self view] addSubview:myLabel];
        [myLabelArray addObject:myLabel];
        x = x+labelXVal + 4;
    }
    
    NSArray *alphabetArray = [NSArray arrayWithObjects: @"a", @"b", @"c", @"d", @"e", @"f", @"g", @"h", @"i", @"j", @"k", @"l", @"m", @"n", @"o", @"p", @"q", @"r", @"s", @"t", @"u", @"v", @"w", @"x", @"y", @"z", nil];


    //==============Setting up the hangman board======================================///
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(100, 65)];
    [path addLineToPoint:CGPointMake(100, 235)];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer.lineWidth = 2.5;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    
    [self.view.layer addSublayer:shapeLayer];

    
    UIBezierPath *path1 = [UIBezierPath bezierPath];
    [path1 moveToPoint:CGPointMake(100, 65)];
    [path1 addLineToPoint:CGPointMake(200, 65)];
    
    CAShapeLayer *shapeLayer1 = [CAShapeLayer layer];
    shapeLayer1.path = [path1 CGPath];
    shapeLayer1.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer1.lineWidth = 2.5;
    shapeLayer1.fillColor = [[UIColor clearColor] CGColor];
    
    [self.view.layer addSublayer:shapeLayer1];
    
    
    UIBezierPath *path2 = [UIBezierPath bezierPath];
    [path2 moveToPoint:CGPointMake(50, 235)];
    [path2 addLineToPoint:CGPointMake(200, 235)];
    
    CAShapeLayer *shapeLayer2 = [CAShapeLayer layer];
    shapeLayer2.path = [path2 CGPath];
    shapeLayer2.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer2.lineWidth = 2.5;
    shapeLayer2.fillColor = [[UIColor clearColor] CGColor];
    
    [self.view.layer addSublayer:shapeLayer2];
    
    
    UIBezierPath *path3 = [UIBezierPath bezierPath];
    [path3 moveToPoint:CGPointMake(200, 65)];
    [path3 addLineToPoint:CGPointMake(200, 100)];
    
    CAShapeLayer *shapeLayer3 = [CAShapeLayer layer];
    shapeLayer3.path = [path3 CGPath];
    shapeLayer3.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer3.lineWidth = 2.5;
    shapeLayer3.fillColor = [[UIColor clearColor] CGColor];
    
    [self.view.layer addSublayer:shapeLayer3];
    
    
    
    /*==================Head==================*/
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(175, 100, 50, 50)]CGPath]];
    circleLayer.strokeColor = [[UIColor whiteColor] CGColor];
    circleLayer.fillColor = [[UIColor clearColor] CGColor];
    circleLayer.lineWidth = 2.5;
    
    //[[self.view layer] addSublayer:circleLayer];
    
    /*==================Body==================*/
    UIBezierPath *path4 = [UIBezierPath bezierPath];
    [path4 moveToPoint:CGPointMake(200, 151)];
    [path4 addLineToPoint:CGPointMake(200, 196)];
    
    CAShapeLayer *shapeLayer4 = [CAShapeLayer layer];
    shapeLayer4.path = [path4 CGPath];
    shapeLayer4.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer4.lineWidth = 2.5;
    shapeLayer4.fillColor = [[UIColor clearColor] CGColor];
   // [self.view.layer addSublayer:shapeLayer4];

    /*==================Left leg==================*/
    UIBezierPath *path5 = [UIBezierPath bezierPath];
    [path5 moveToPoint:CGPointMake(200, 195)];
    [path5 addLineToPoint:CGPointMake(175, 215)];
    
    CAShapeLayer *shapeLayer5 = [CAShapeLayer layer];
    shapeLayer5.path = [path5 CGPath];
    shapeLayer5.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer5.lineWidth = 2.5;
    shapeLayer5.fillColor = [[UIColor clearColor] CGColor];
   // [self.view.layer addSublayer:shapeLayer5];
    
    
    /*==================Right leg==================*/
    UIBezierPath *path6 = [UIBezierPath bezierPath];
    [path6 moveToPoint:CGPointMake(200, 195)];
    [path6 addLineToPoint:CGPointMake(225, 215)];
    
    CAShapeLayer *shapeLayer6 = [CAShapeLayer layer];
    shapeLayer6.path = [path6 CGPath];
    shapeLayer6.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer6.lineWidth = 2.5;
    shapeLayer6.fillColor = [[UIColor clearColor] CGColor];
   // [self.view.layer addSublayer:shapeLayer6];
    
    /*==================Left arm==================*/
    UIBezierPath *path7 = [UIBezierPath bezierPath];
    [path7 moveToPoint:CGPointMake(200, 160)];
    [path7 addLineToPoint:CGPointMake(175, 145)];
    
    CAShapeLayer *shapeLayer7 = [CAShapeLayer layer];
    shapeLayer7.path = [path7 CGPath];
    shapeLayer7.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer7.lineWidth = 2.5;
    shapeLayer7.fillColor = [[UIColor clearColor] CGColor];
   // [self.view.layer addSublayer:shapeLayer7];
    
    /*==================Right arm==================*/
    UIBezierPath *path8 = [UIBezierPath bezierPath];
    [path8 moveToPoint:CGPointMake(200, 160)];
    [path8 addLineToPoint:CGPointMake(225, 145)];
    
    CAShapeLayer *shapeLayer8 = [CAShapeLayer layer];
    shapeLayer8.path = [path8 CGPath];
    shapeLayer8.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer8.lineWidth = 2.5;
    shapeLayer8.fillColor = [[UIColor clearColor] CGColor];
  //  [self.view.layer addSublayer:shapeLayer8];
    
    /*==================Left foot==================*/
    UIBezierPath *path9 = [UIBezierPath bezierPath];
    [path9 moveToPoint:CGPointMake(175, 215)];
    [path9 addLineToPoint:CGPointMake(160, 205)];
    
    CAShapeLayer *shapeLayer9 = [CAShapeLayer layer];
    shapeLayer9.path = [path9 CGPath];
    shapeLayer9.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer9.lineWidth = 2.5;
    shapeLayer9.fillColor = [[UIColor clearColor] CGColor];
   // [self.view.layer addSublayer:shapeLayer9];
    
    
    /*==================Right foot==================*/
    UIBezierPath *path10 = [UIBezierPath bezierPath];
    [path10 moveToPoint:CGPointMake(225, 215)];
    [path10 addLineToPoint:CGPointMake(240, 205)];
    
    CAShapeLayer *shapeLayer10 = [CAShapeLayer layer];
    shapeLayer10.path = [path10 CGPath];
    shapeLayer10.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer10.lineWidth = 2.5;
    shapeLayer10.fillColor = [[UIColor clearColor] CGColor];
    //[self.view.layer addSublayer:shapeLayer10];
    
    
    //Array for the hangman's drawing actions
    shapeArray = [NSArray arrayWithObjects:circleLayer, shapeLayer4, shapeLayer5, shapeLayer6, shapeLayer7, shapeLayer8, shapeLayer9, shapeLayer10, nil];

    
    /*===========================================================================================*/
    
    myButtonArray = [NSMutableArray array];
    int charIndex = 0;
    int xCoor = 13;
    int yCoor = 350;
    
    //create array of button letters
    for (int i = 0; i < 24; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(xCoor, yCoor, 33, 33);
        NSString *name = [alphabetArray objectAtIndex:charIndex];
        [button setTitle: name forState: UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:23];

        [[button layer] setBorderWidth:2.0f];
        [[button layer] setBorderColor:[UIColor whiteColor].CGColor];
        
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [myButtonArray addObject:button];
        [self.view addSubview:button];

        charIndex++;
        xCoor+=50;
        if (xCoor > 300) {
            xCoor = 13;
            yCoor += 45;
        }
        
        UIButton *buttonY = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonY.frame = CGRectMake(113, 530, 33, 33);
       // NSString *name = [alphabetArray objectAtIndex:charIndex];
        [buttonY setTitle: @"y" forState: UIControlStateNormal];
        buttonY.titleLabel.font = [UIFont systemFontOfSize:23];
        
        [[buttonY layer] setBorderWidth:2.0f];
        [[buttonY layer] setBorderColor:[UIColor whiteColor].CGColor];
        
        [buttonY addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [myButtonArray addObject:buttonY];
        [self.view addSubview:buttonY];
        
        UIButton *buttonZ = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonZ.frame = CGRectMake(163, 530, 33, 33);
        // NSString *name = [alphabetArray objectAtIndex:charIndex];
        [buttonZ setTitle: @"z" forState: UIControlStateNormal];
        buttonZ.titleLabel.font = [UIFont systemFontOfSize:23];
        
        [[buttonZ layer] setBorderWidth:2.0f];
        [[buttonZ layer] setBorderColor:[UIColor whiteColor].CGColor];
        
        [buttonZ addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [myButtonArray addObject:buttonZ];
        [self.view addSubview:buttonZ];
    }
    
    
    
    
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)buttonPressed:(UIButton *)sender {
    BOOL foundLetter = false;
    NSString * name = [sender currentTitle];
    
    NSString * charString;

    for (NSInteger charIndex=0; charIndex < wordLength; charIndex++) {
        charString = [NSString stringWithFormat:@"%c" , [wordToGuess characterAtIndex:charIndex]];

        
        if ([name isEqual:charString]) {

            
            UILabel *myLabel = [myLabelArray objectAtIndex:charIndex];
            myLabel.text = name;
            [myLabel setFont:[UIFont systemFontOfSize:23]];
            [[self view] addSubview:myLabel];
            foundLetter = true;
            numLettersFound++;
            if (numLettersFound == wordLength) {
                 PhewViewController * pvc = [self.storyboard instantiateViewControllerWithIdentifier:@"phew"];
                 [pvc setModalPresentationStyle:UIModalPresentationFullScreen];
                 [self presentViewController:pvc animated:YES completion:nil];
            }
            
        }
        
    }
    if (!foundLetter) {
        numGuesses--;
        NSString* guessesString = [NSString stringWithFormat:@"Guesses remaining: %d", numGuesses];
        guessesRemainingLabel.text = guessesString;
        [self.view.layer addSublayer:[shapeArray objectAtIndex:shapeIndex]];
        shapeIndex++;
        if (numGuesses == 0) {

            GameOverViewController * gvc = [self.storyboard instantiateViewControllerWithIdentifier:@"gameover"];
            [gvc setModalPresentationStyle:UIModalPresentationFullScreen];
            [self presentViewController:gvc animated:YES completion:nil];

        }


    }
}


- (IBAction)submitGuess:(id)sender {
    NSLog(@"Called guess word");
    NSString * attemptedWord = guessWordTextbox.text;
    NSLog(attemptedWord);
    NSLog(wordToGuess);
    if([attemptedWord isEqual: wordToGuess]) {
        PhewViewController * pvc = [self.storyboard instantiateViewControllerWithIdentifier:@"phew"];
        [pvc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:pvc animated:YES completion:nil];
    } else {
        numGuesses--;
        if (numGuesses == 0) {
            GameOverViewController * gvc = [self.storyboard instantiateViewControllerWithIdentifier:@"gameover"];
            [gvc setModalPresentationStyle:UIModalPresentationFullScreen];
            [self presentViewController:gvc animated:YES completion:nil];
            
        }
    }
}
@end


