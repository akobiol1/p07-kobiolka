//
//  FriendsViewController.h
//  NewHangman
//
//  Created by Amanda Kobiolka on 4/29/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface FriendsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *textBox;

- (IBAction)submit:(id)sender;

- (IBAction)backButton:(id)sender;

@end
