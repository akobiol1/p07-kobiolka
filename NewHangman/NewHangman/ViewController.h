//
//  ViewController.h
//  NewHangman
//
//  Created by Amanda Kobiolka on 4/28/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameOverViewController.h"
#import "FriendsViewController.h"

@interface ViewController : UIViewController
- (IBAction)playWithAFriendButton:(id)sender;

- (IBAction)noFriendsButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *friendButton;
@property (strong, nonatomic) IBOutlet UIButton *noFriendButton;

@end

