//
//  FriendsViewController.m
//  NewHangman
//
//  Created by Amanda Kobiolka on 4/29/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FriendsViewController.h"
#import "HangmanViewController.h"
#import "ViewController.h"

@interface FriendsViewController ()

@end

@implementation FriendsViewController : UIViewController

@synthesize textBox;

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chalkboardBackground.jpg"]];

    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submit:(id)sender {
 }

- (IBAction)backButton:(id)sender {
    ViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewContrl"];
    [vc setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:vc animated:YES completion:nil];
}


 -(void) prepareForSegue:(UIStoryboardSegue *) segue sender:(id)sender {
 HangmanViewController *hvc;
 hvc = [segue destinationViewController];
 hvc.userSubmittedWord = textBox.text;
 
 
 }

- (IBAction)playWithAFriendButton:(id)sender {
}

- (IBAction)noFriendsButton:(id)sender {
}

@end
