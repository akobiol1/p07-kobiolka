//
//  GameOverViewController.m
//  NewHangman
//
//  Created by Amanda Kobiolka on 4/30/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GameOverViewController.h"


@interface GameOverViewController ()

@end

@implementation GameOverViewController : UIViewController


- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chalkboardBackground.jpg"]];

    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
