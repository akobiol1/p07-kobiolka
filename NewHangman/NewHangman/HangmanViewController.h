//
//  Header.h
//  NewHangman
//
//  Created by Amanda Kobiolka on 4/28/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

/* Header_h */


#import <UIKit/UIKit.h>

@interface HangmanViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *guessWordTextbox;
@property (strong, nonatomic) IBOutlet NSMutableArray *myLabelArray;
@property (weak, nonatomic) IBOutlet NSMutableArray *myButtonArray;
@property (strong, nonatomic) IBOutlet NSArray *shapeArray;
@property (weak, nonatomic) NSString * userSubmittedWord;
@property (strong, nonatomic) IBOutlet UILabel *guessesRemainingLabel;


- (IBAction)submitGuess:(id)sender;

@end
