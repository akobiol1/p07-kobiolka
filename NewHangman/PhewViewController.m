//
//  PhewViewController.m
//  NewHangman
//
//  Created by Amanda Kobiolka on 4/30/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "FriendsViewController.h"
#import "HangmanViewController.h"
#import "ViewController.h"
#import "PhewViewController.h"

@interface PhewViewController ()

@end

@implementation PhewViewController : UIViewController

@synthesize textBox;

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"chalkboardBackground.jpg"]];

    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end